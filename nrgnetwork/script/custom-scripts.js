console.log("OI! EU SOU UM ATALHO PRO DESENVOLVEDOR FUÇAR NO JAVA SCRIPT... :)");

// RESPONSIVE SCRIPTS
var width = jQuery(document).width();
if (width <= 1200) {
	$("#sidebar_filter").css("display", "none");
	$( "#main-content-panel .pull-right" ).removeClass("col-md-10 pull-right");
} else {
	$("#sidebar_filter").css("display", "initial");
}

// SET COOKIES FUNCTION
function setCookie(cookieName, cookieValue, expirationDays) {
	var d = new Date();
	d.setTime(d.getTime() + (expirationDays * 24 * 60 * 60 * 1000));
	var expires = "expires="+d.toUTCString();
	document.cookie = cookieName + "=" + cookieValue + ";" + expires + ";path=/";
}
// GET COOKIES FUNCTION
function getCookie(name) {
	var dc = document.cookie;
	var prefix = name + "=";
	var begin = dc.indexOf("; " + prefix);
	if (begin == -1) {
		begin = dc.indexOf(prefix);
		if (begin != 0) return null;
	}
	else
	{
		begin += 2;
		var end = document.cookie.indexOf(";", begin);
		if (end == -1) {
			end = dc.length;
		}
	}
	// because unescape has been deprecated, replaced with decodeURI
	//return unescape(dc.substring(begin + prefix.length, end));
	return decodeURI(dc.substring(begin + prefix.length, end));
}

// INSTAGRAM MODAL COOKIE
var hasInstaModalCookie = 1;
var instaModalCookieExpiration = 7;

function setInstaModalCookie() {
	setCookie("InstaModalCookie", hasInstaModalCookie, instaModalCookieExpiration);
}

function showPopup() {
	jQuery("#instagrammodal").css("display", "initial");
}

jQuery(document).ready(function() {
	if (getCookie('InstaModalCookie') == null) {
		window.setTimeout( showPopup, 30000 ); // 30 seconds
	}
});
// CLOSE MODAL
window.onload = function () {
    	document.getElementById('closemodal').onclick = function () {
        document.getElementById('instagrammodal').style.display = "none";
    	setInstaModalCookie();
    };
};
